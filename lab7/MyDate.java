public class MyDate {
    private int Day, Month, Year;
    int[] mxDays = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public MyDate(int Day, int Month, int Year) {
        this.Day = Day;
        this.Month = Month - 1;
        this.Year = Year;
    }

    public MyDate(MyDate dt) {
        this(dt.Year, dt.Month, dt.Day);
    }

    public String toString() {
        return Year + "-" + ((Month+1) < 10 ? "0" : "") + (Month+1) + "-" + (Day < 10 ? "0" : "") + Day;
    }

    public void incrementDay() {
        int newDay = Day + 1;
        int mxDay = mxDays[Month];
        if (newDay > mxDay) {
            incrementMonth();
            Day = 1;
        } else if (Month == 1 && newDay == 29 && !LeapYear()) {
            Day = 1;
            incrementMonth();
        } else {
            Day = newDay;
        }
    }

    private boolean LeapYear() {
        return Year % 4 == 0 ? true : false;
    }

    public void incrementYear(int diff) {
        Year += diff;
        if (Month == 1 && Day == 29 && !LeapYear()) {
            Day = 28;
        }
    }

    public void decrementDay() {
        int newDay = Day - 1;
        if (newDay == 0) {
            Day = 31;
            decrementMonth();
        } else {
            Day = newDay;
        }
    }
    public void decrementYear() {
        decrementYear(1);
    }

    public void decrementMonth() {
        incrementMonth(-1);
    }

    public void incrementDay(int diff) {
        while (diff > 0) {
            incrementDay();
            diff--;
        }
    }

    public void decrementMonth(int Month) {
        incrementMonth(-Month);
    }

    public void decrementDay(int diff) {
        while (diff > 0) {
            decrementDay();
            diff--;
        }
    }
    public void incrementMonth(int diff) {
        int newMonth = (Month + diff) % 12;
        int yearDiff = 0;
        if (newMonth < 0) {
            newMonth = newMonth + 12;
            yearDiff = -1;
        }
        yearDiff += (Month + diff) / 12;

        Month = newMonth;
        Year += yearDiff;

        if (Day > mxDays[Month]) {
            Day = mxDays[Month];
            if (Month == 1 && Day == 29 && !LeapYear()) {
                Day = 28;
            }
        }
    }

    public void decrementYear(int year) {
        incrementYear(-year);
    }

    public void incrementMonth() {
        incrementMonth(1);
    }

    public void incrementYear() {
        incrementYear(1);
    }

    public boolean isBefore(MyDate anotherDate) {
        if (dayCount(this) < dayCount(anotherDate)) return true;
        return false;
    }

    public boolean isAfter(MyDate anotherDate) {
        if (dayCount(this) > dayCount(anotherDate)) return true;
        return false;
    }

    public int dayDifference(MyDate anotherDate) {
        return Math.abs(dayCount(this) - dayCount(anotherDate) - 2);
    }

    private int dayCount(MyDate dt) {
        return dt.Year * 365 + dt.Month * 30 + dt.Day;
    }
}
