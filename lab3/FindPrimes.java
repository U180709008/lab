public class FindPrimes {
    public static void main(String[] args){
        int max = Integer.parseInt(args[0]);
        for (int number = 2; number < max; number++){ 
            int divisor = 2; // Let divisor = 2;
            boolean isPrime = true; //Let isPrime = true;
            while (divisor < number && isPrime){
                if ( number % divisor == 0){ 
                    isPrime = false; //isPrime =false;
                }
                divisor++; //increment divisor;
            }
            if (isPrime){ //If the isPrime is (still) true
                System.out.print(number + ",");
            }
        }




    }
}
