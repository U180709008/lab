import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {
	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		Random rand =new Random();
		int number = rand.nextInt(100);
		
		System.out.println("Hi! I am thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
		
		int guess = reader.nextInt();
		int count = 1;
		while (guess != number && guess != -1) {
			System.out.println("Sorry!");
			
			if (guess != -1) {
				if (guess < number) {
					System.out.println("Mine is greater than your guess");
					
				}else {
					System.out.println("Mine is less than your guess");
					
				}
				System.out.println("Type -1 to quit or guess another: ");
				guess = reader.nextInt();
				count++;
			}
		}
		if (number == guess) {
			System.out.println("Congularations! You won after " + count + " attempts");
			
			
		}else {
			System.out.println("Sorry the number was " + number);
			
		}
		reader.close();
	}
}